import pygame, pdb
from pygame.locals import *
from tools import pg_caption

class Player(object):
    """creates a player that the user can control"""
    def __init__(self, screen, surfaces):
        self.screen = screen

        #player dimentions pooppoop
        self.x = 100
        self.y = 575
        self.width_init = 25
        self.height_init = 25
        self.width = self.width_init
        self.height = self.height_init

        #movement bool
        self.move_right = False
        self.move_left = False
        self.move = False
        self.jump = False
        self.duck = False
        self.under_surface = False

        #movement increments
        self.speed = 0
        self.up = None
        self.num = None
        self.num1 = 1
        self.max_speed = 5

        #all delays
        self.deceleration_delay_init = 5
        self.deceleration_delay = self.deceleration_delay_init
        self.move_delay_init = 3
        self.move_delay = self.move_delay_init
        self.jump_up_delay_init = 10
        self.jump_up_delay = self.jump_up_delay_init
        self.jump_down_delay_init = 8
        self.jump_down_delay = self.jump_down_delay_init
        self.duck_delay_init = 2
        self.duck_delay = self.duck_delay_init

        #surface list
        self.surfaces = surfaces

    def draw(self):
        """draws player"""
        pygame.draw.rect(self.screen, (0, 0, 255), (self.x, self.y, self.width, self.height))

    def movement(self):
        """produces constant movement"""
        if self.x > 0 and self.x < self.screen.get_width():
            self.x = self.x + self.speed
        else:
            if self.x <= 1:
                self.x = (self.screen.get_width() - self.width)
            elif self.x >= (self.screen.get_width() - self.width):
                self.x = 1
        try:
            self.y = self.y - self.up
        except Exception, e:
            pass

    def accelerate_movement(self):
        """handles acceleration of player"""
        if self.move_right == True or self.move_left == True:
            self.move_delay = self.move_delay - 1
            if self.move_delay == 0:
                self.speed = self.speed + self.num1
                self.move_delay = self.move_delay_init
                if self.speed > self.max_speed:
                    self.speed = self.max_speed
                if self.speed < -self.max_speed:
                    self.speed = -self.max_speed

    def decelerate_movement(self):
        """handles deceleration of player"""
        if self.move_right == False and self.move_left == False:
            if self.speed != 0:
                self.deceleration_delay = self.deceleration_delay - 1
                if self.deceleration_delay == 0:
                    if self.speed > 0:
                        self.speed = self.speed - 1
                    else:
                        self.speed = self.speed + 1
                    self.deceleration_delay = self.deceleration_delay_init

    def jump_up_animation(self):
        """handles players jump up animation"""
        if self.jump == True:
            if self.up != None:
                self.jump_up_delay = self.jump_up_delay - 1
                if self.jump_up_delay == 0:
                    self.up = self.up - 1
                    self.jump_up_delay = self.jump_up_delay_init
                if self.up == 0:
                    self.up = 0
                    self.jump = False
            else:
                self.up = 4

    def jump_down_animation(self):
        """handles players falling animation(jump down)"""
        if self.up != None and self.jump == False:
            if self.num == None:
                self.num = 1
            self.jump_down_delay = self.jump_down_delay - 1
            if self.jump_down_delay == 0:
                self.up = self.up - self.num
                self.num = self.num + 1
                self.jump_down_delay = self.jump_down_delay_init

        if self.y > (self.screen.get_height() - self.height):
            self.y = (self.screen.get_height() - self.height)
            self.up = None
            self.num = None

    def duck_animation(self):
        """places player in a duck animation"""
        if self.duck == True:
            if self.up > 3:
                self.up = 2
            self.move_delay_init = 10
            self.duck_delay = self.duck_delay - 1
            if self.duck_delay == 0:
                if self.max_speed != 2:
                    self.max_speed =  self.max_speed - 1
                self.height = self.height - 2
                self.width = self.width + 1
                self.duck_delay = self.duck_delay_init
                if self.width > 35:
                    self.width = 35
                if self.height < 15:
                    self.height = 15
                else:
                    self.y = self.y + 2

    def duck_revert(self):
        """reverts players animation from duck position to stand"""
        if self.duck == False:
            self.move_delay_init = 3
            self.duck_delay = self.duck_delay - 1
            if self.duck_delay <= 0:
                if self.check_duck_sustain() == False:
                    self.max_speed = 5
                    self.height = self.height + 2
                    self.width = self.width - 1
                    self.duck_delay = self.duck_delay_init
                    if self.width < self.width_init:
                        self.width = self.width_init
                    if self.height > self.height_init:
                        self.height = self.height_init
                    else:
                        self.y = self.y - 2
            pg_caption(self.check_duck_sustain(), self.duck_delay, self.duck)

    def check_duck_sustain(self):
        """checks if the player is crouched and under a surface that requires a sustained duck"""
        if self.duck == False:
            for surface in self.surfaces:
                if self.y > surface.y + surface.height:
                    distance = self.y - (surface.y + surface.height)
                    if distance < 10:
                        if self.x in surface.width_range:
                            self.up = None
                            return True
        return False

    def update(self):
        """updates neccesary methods"""
        self.draw()
        self.movement()
        self.accelerate_movement()
        self.decelerate_movement()
        self.jump_up_animation()
        self.jump_down_animation()
        self.duck_animation()
        self.duck_revert()
        self.check_duck_sustain()
