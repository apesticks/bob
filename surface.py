import pygame, pdb
from pygame.locals import *

class Surface(object):
    """creates platforms for a player to jump on"""
    def __init__(self, screen, x, y, w, h):
        self.screen = screen

        #surface dimentions
        self.x = x
        self.y = y
        self.height = h
        self.width = w

        #ranges
            #number 25 is hard coded, shouldnt be
        self.width_range = range(self.x - 25, self.x + self.width)
        self.height_range = range(self.y, self.y + self.height)
        self.check = False

    def draw(self):
        """draws a surface on the screen with passed in (x, y, w, h)"""
        pygame.draw.rect(self.screen, (200, 200, 200), (self.x, self.y, self.width, self.height))

    def top_collision(self, collider):
        """handles collision with the top of a surface"""
        if collider.y + collider.height <= self.y:
            if collider.x in self.width_range:
                self.check = True
            else:
                self.check = False
                if collider.up == None:
                    collider.up = -1
                if collider.y > (self.screen.get_height() - collider.height):
                    collider.y = (self.screen.get_height() - collider.height)

        if self.check == True:
            if collider.y + collider.height > self.y:
                collider.y = self.y - collider.height
                collider.jump = False
                collider.up = None
                collider.num = None
                self.check = False

    def bottom_collision(self, collider):
        """handles collision with the bottom of a surface"""
        if collider.y < self.y + self.height and collider.y > self.y + self.height - 5:
            if collider.x in self.width_range:
                collider.y = self.y + self.height
                collider.up = 0

    def right_collision(self, collider):
        """handles collision with right side of a surface"""
        collider_range = range(collider.y, collider.y + collider.height)
        if collider.x < self.x + self.width and collider.x > self.x + (self.width)/2:
            for num in collider_range:
                if num in self.height_range:
                    collider.speed = 0
                    collider.x = self.x + self.width

    def left_collision(self, collider):
        """handles collision with left side of a surface"""
        collider_range = range(collider.y, collider.y + collider.height)
        if collider.x + collider.width in range(self.x, self.x + (self.width)/2):
            for num in collider_range:
                if num in self.height_range:
                    collider.speed = 0
                    collider.x = self.x - collider.width

    def update(self, collider):
        """updates neccesary methods"""
        self.draw()
        self.top_collision(collider)
        self.bottom_collision(collider)
        self.right_collision(collider)
        self.left_collision(collider)