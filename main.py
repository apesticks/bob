import pygame, sys, pdb
from pygame.locals import *
from game import Game

def main():
    game = Game()
    game.run()

main()
