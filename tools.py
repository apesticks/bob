import pygame

def pg_caption(*captions):
    cap = ''
    for caption in captions:
        cap = cap + str(caption)

    pygame.display.set_caption(cap)