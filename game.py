import pygame, sys, pdb, os
from pygame.locals import *
from player import Player
from surface import Surface

class Game(object):
    def __init__(self, screen_size=(800,600)):
        pygame.init()
        self.running = True
        self.fps = pygame.time.Clock()

        #sets the screen to apear in same position everytime
        self.window_position = 1000, 400
        os.environ["SDL_VIDEO_WINDOW_POS"] = str(self.window_position[0]) + "," + str(self.window_position[1])
        self.screen_size = screen_size
        self.screen = pygame.display.set_mode(self.screen_size ,0 , 32)

        #class instantiations
        self.surface1 = Surface(self.screen, 0, 560, 150, 20)
        self.surface2 = Surface(self.screen, 300, 500, 50, 20)
        self.surfaces = [self.surface1, self.surface2]
        self.player = Player(self.screen, self.surfaces)

    def run(self):
        """handles game loop"""
        while self.running:
            self.fps.tick(60)
            self.events()
            self.screen.fill((0,0,0))
            self.player.update()
            for surface in self.surfaces:
                surface.update(self.player)
            pygame.display.update()

    def events(self):
        """handles game events(keyboard)"""
        for event in pygame.event.get():
            if event.type == QUIT:
                self.running = False
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    self.running = False
                if event.key == K_t:
                    print "ok"

                if event.key == K_d:
                    self.player.move_right = True
                    self.player.num1 = 1
                if event.key == K_a:
                    self.player.move_left = True
                    self.player.num1 = -1
                if event.key == K_SPACE:
                    if self.player.up == None:
                          self.player.jump = True
                if event.key == K_s:
                    self.player.duck = True

            if event.type == KEYUP:
                if event.key == K_d:
                    self.player.move_right = False
                    self.player.num1 = -1
                if event.key == K_a:
                    self.player.move_left = False
                    self.player.num1 = 1
                if event.key == K_SPACE:
                    self.player.jump = False
                if event.key == K_s:
                    self.player.duck = False